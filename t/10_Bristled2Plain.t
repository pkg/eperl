# SPDX-License-Identifier: 0BSD
require "TEST.pl";
TEST::init();

print "1..1\n";

$testinput = <<'EOT'
<:=$ENV{SCRIPT_SRC_PATH}_:>
<:=$ENV{SCRIPT_SRC_PATH};:>
<:=$ENV{SCRIPT_SRC_PATH}:>
hehe
<: rest will be skipped :>// shouldn't be there
bebe
SCRIPT_SRC_PATH_FILE=	<: print $ENV{SCRIPT_SRC_PATH_FILE}
q

:>
EOT
;
$testinput =~ s/\n/\\n/g;
$testinput =~ s/"/\\"/g;

$SIG{CHLD} = "DEFAULT";
$prog = TEST::tmpfile();
open($progpipe, "|-", "\${CC-cc} -o $prog -I.. ../eperl_parse.o -xc -");
print $progpipe <<"EOT"
#include "eperl.h"
int main() {
	puts(ePerl_Sprinkled2Plain(strdup("$testinput"), "<:", ":>", true, false));
}
EOT
;
close($progpipe);
wait;
$got = `./$prog`;

$want = <<'EOT'
print $ENV{SCRIPT_SRC_PATH} print "\n";
print $ENV{SCRIPT_SRC_PATH}; print "\n";
print $ENV{SCRIPT_SRC_PATH}; print "\n";
print "hehe\n";
rest will be skipped;
print "bebe\n";
print "SCRIPT_SRC_PATH_FILE=\t"; print $ENV{SCRIPT_SRC_PATH_FILE}
q;

print "\n";

EOT
;
$want =~ s/skipped;/skipped; /;
if($want eq $got) {
	print "ok\n";
} else {
	print "not ok\n";
	print "want=>>>$want<<<\n";
	print "got =>>>$got<<<\n";
}

TEST::cleanup();
