# SPDX-License-Identifier: Artistic-1.0-Perl OR GPL-2.0-only
require "TEST.pl";
&TEST::init;

print "1..6\n";

#   setup test files

$testfile1 = &TEST::tmpfile_with_name("page.html", <<"EOT"
some stuff
some more stuff
EOT
);

$testfile1b = &TEST::tmpfile(<<"EOT"
HTTP/1.0 200 OK\r
Server: XXXX\r
Date: XXXX\r
Connection: close\r
Content-Type: text/html\r
Content-Length: 27\r
\r
some stuff
some more stuff
EOT
);
$testfile2 = &TEST::tmpfile_with_name("page2.html", <<"EOT"
some stuff
<? print "foo bar"; !>
some more stuff
EOT
);
$testfile3 = &TEST::tmpfile(<<"EOT"
HTTP/1.0 200 OK\r
Server: XXXX\r
Date: XXXX\r
Connection: close\r
Content-Type: text/html\r
Content-Length: 35\r
\r
some stuff
foo bar
some more stuff
EOT
);

#   test for working forced NPH-CGI mode
$tempfile1 = &TEST::tmpfile;
$rc = &TEST::system("../eperl -m n $testfile1 | sed -e 's/^Server:.*\r/Server: XXXX\r/' -e 's/^Date:.*\r/Date: XXXX\r/' >$tempfile1");
print ($rc == 0 ? "ok\n" : "not ok\n");
$rc = &TEST::system("cmp $testfile1b $tempfile1");
print ($rc == 0 ? "ok\n" : "not ok\n");

#   test for working implicit CGI mode
$tempfile2 = &TEST::tmpfile;
$rc = &TEST::system("PATH_TRANSLATED=$testfile1; export PATH_TRANSLATED; GATEWAY_INTERFACE=CGI/1.1; export GATEWAY_INTERFACE; ../eperl -m n | sed -e 's/^Server:.*\r/Server: XXXX\r/' -e 's/^Date:.*\r/Date: XXXX\r/' >$tempfile2");
print ($rc == 0 ? "ok\n" : "not ok\n");
$rc = &TEST::system("cmp $testfile1b $tempfile2");
print ($rc == 0 ? "ok\n" : "not ok\n");

#   test if both are equal
$rc = &TEST::system("cmp $tempfile1 $tempfile2");
print ($rc == 0 ? "ok\n" : "not ok\n");

#   test if filter mode actually works for embedded Perl 5 blocks
$tempfile3 = &TEST::tmpfile;
&TEST::system("../eperl -m n $testfile2 | sed -e 's/^Server:.*\r/Server: XXXX\r/' -e 's/^Date:.*\r/Date: XXXX\r/' >$tempfile3");
$rc = &TEST::system("cmp $tempfile3 $testfile3");
print ($rc == 0 ? "ok\n" : "not ok\n");

&TEST::cleanup;
