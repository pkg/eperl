# SPDX-License-Identifier: Artistic-1.0-Perl OR GPL-2.0-only
dnl ##  Copyright (c) 1996-1999 Ralf S. Engelschall, <rse@engelschall.com>

AC_PREREQ([2.71])
AC_INIT
EPERL_VERSION_SHORT='2.2.15'
EPERL_VERSION_DATE='2024-10-19'
echo "Configuring for ePerl, Version $EPERL_VERSION_SHORT ($EPERL_VERSION_DATE)"
AC_SUBST(EPERL_VERSION_SHORT)
AC_SUBST(EPERL_VERSION_DATE)
AC_DEFINE_UNQUOTED(EPERL_VERSION_SHORT, "$EPERL_VERSION_SHORT", [ePerl version])
AC_DEFINE_UNQUOTED(EPERL_VERSION_DATE,  "$EPERL_VERSION_DATE",  [ePerl version date])

AC_CONFIG_HEADERS([config.h])

dnl #
dnl #   libdir adjustment
dnl #

test "x$prefix" = xNONE && prefix=$ac_default_prefix
eval "dir=$prefix"
case $dir in
    *eperl* ) libsubdir= ;;
          * ) libsubdir="/eperl" ;;
esac
AC_SUBST(libsubdir)

dnl #
dnl #   latest find Perl interpreter
dnl #

AC_MSG_RESULT(CHECK: Configuration of Perl Language)
AC_MSG_CHECKING([for Perl language])
AC_ARG_WITH(perl,dnl
[  --with-perl=PATH        force the usage of a specific Perl 5 interpreter],
perl_prog=$with_perl
,
perl_prog=$(command -v perl || command -v perl5 || command -v miniperl)
)dnl
dnl [[
perl_vers="$($perl_prog -e 'printf("%.3f", $]);')"
dnl ]
AC_MSG_RESULT([$perl_prog v$perl_vers])
if ! test -f "$perl_prog"; then
    AC_MSG_ERROR(required program perl not found)
fi


AC_MSG_CHECKING([which UIDs/usernames to allow when running set-UID])
AC_ARG_WITH(allowed-caller-uids,dnl
[  --with-allowed-caller-uids=username,uid,...  when running set-UID, refuse to switch user IDs (or exit) if it's not in this list (default: "nobody, root")],
[],
[with_allowed_caller_uids='nobody, root']
)dnl
AC_MSG_RESULT([$with_allowed_caller_uids])
ALLOWED_CALLER_UID="$(IFS="$IFS,"; printf '"%s", ' $with_allowed_caller_uids)"
AC_DEFINE_UNQUOTED(ALLOWED_CALLER_UID, $ALLOWED_CALLER_UID, [initialiser for allowed_caller_uid])
AC_SUBST(with_allowed_caller_uids)

dnl #
dnl #   determine Perl parameters
dnl #

AC_MSG_CHECKING([for Perl knowledge of system])
perl_os="$($perl_prog -e 'use Config; print "$Config{osname}-$Config{osvers}"')"
AC_MSG_RESULT([$perl_os])

AC_MSG_CHECKING([for Perl standard compiler])
perl_cc="$($perl_prog -e 'use Config; print $Config{cc}')"
if test -z "$CC"; then
    CC=$perl_cc
    export CC
    AC_MSG_RESULT([$perl_cc])
else
    perl_cc=$CC
    AC_MSG_RESULT([$perl_cc (OVERWRITTEN)])
fi
AC_SUBST(perl_cc)

AC_MSG_CHECKING([for Perl standard optimization flags])
perl_optimize="$($perl_prog -e 'use Config; print $Config{optimize}')"
AC_MSG_RESULT([$perl_optimize])
AC_SUBST(perl_optimize)

AC_MSG_CHECKING([for Perl standard compilation flags])
perl_ccflags="$($perl_prog -e 'use Config; print $Config{ccflags}')"
case $perl_os in
    *hpux*    ) perl_ccflags="$perl_ccflags -Wp,-H32768" ;;
esac
AC_MSG_RESULT([$perl_ccflags])
AC_SUBST(perl_ccflags)

AC_MSG_CHECKING([for Perl standard link flags])
perl_ldflags="$($perl_prog -e 'use Config; print $Config{ldflags}')"
AC_MSG_RESULT([$perl_ldflags])
AC_SUBST(perl_ldflags)

AC_MSG_CHECKING([for Perl library files])
perl_libs="$($perl_prog -e 'use Config; print $Config{libs} =~ s/-l\S*db\S*//gr =~ s/^\s*//r')"
AC_MSG_RESULT([$perl_libs])
AC_SUBST(perl_libs)

AC_MSG_CHECKING([for Perl architecture directory])
perl_archlib="$($perl_prog -e 'use Config; print $Config{archlib}')"
AC_MSG_RESULT([$perl_archlib])
AC_SUBST(perl_archlib)

AC_MSG_CHECKING([for Perl dynamic loading support])
usedl="$($perl_prog -e 'use Config; print $Config{usedl}')"
case $usedl in
    define )
        AC_MSG_RESULT([yes])
        AC_DEFINE(HAVE_PERL_DYNALOADER, 1, defined if Perl support the DynLoader interface for dynamic library loading)
        perl_dla="$($perl_prog -MExtUtils::Embed -e ldopts)"
        ;;
    * )
        AC_MSG_RESULT([no])
        perl_dla=
        ;;
esac
AC_SUBST(perl_dla)

AC_MSG_CHECKING([for Perl dynamic loading compilation flags])
perl_cccdlflags="$($perl_prog -e 'use Config; print $Config{cccdlflags}')"
case $perl_cccdlflags in
    " " ) x="none" ;;
    * )   x="$perl_cccdlflags" ;;
esac
AC_MSG_RESULT([$x])
AC_SUBST(perl_cccdlflags)

AC_MSG_CHECKING([for Perl dynamic loading link flags])
perl_ccdlflags="$($perl_prog -e 'use Config; print $Config{ccdlflags}')"
case $perl_os in
    *aix* ) perl_ccdlflags="$(echo $perl_ccdlflags | sed -e 's;-bE:perl.exp;-bE:${perl_archlib}/CORE/perl.exp;')" ;;
esac
case $perl_ccdlflags in
    " " ) AC_MSG_RESULT([none]) ;;
    * )   AC_MSG_RESULT([$perl_ccdlflags]) ;;
esac

AC_SUBST(perl_ccdlflags)

AC_SUBST(perl_prog)
AC_DEFINE_UNQUOTED(AC_perl_prog,      "$perl_prog",    Perl path)
AC_DEFINE_UNQUOTED(AC_perl_vers,      "$perl_vers",    Perl version)
AC_DEFINE_UNQUOTED(AC_perl_archlib,   "$perl_archlib", Perl architecture directory)
AC_DEFINE_UNQUOTED(AC_perl_libs,      "$perl_libs",    Perl library files)
AC_DEFINE_UNQUOTED(AC_perl_dla,       "$perl_dla",     Perl dynamic loading support)
AC_DEFINE_UNQUOTED(DO_NEWXS_STATIC_MODULES, $($perl_prog eperl_perl5_sm.pl), [dynamic module initialisation])


dnl #
dnl #   determine build tools and parameters
dnl #

AC_MSG_RESULT(CHECK: System Build Environment)
AC_PROG_CC

CFLAGS="$CFLAGS -Wall -Wextra -Wmissing-prototypes -Wmissing-declarations"

AC_CHECK_PROG([AR],[ar],[ar],[])
AC_PROG_MAKE_SET

AC_CHECK_LIB(pthread, pthread_join)


AC_CHECK_FUNCS_ONCE(setproctitle)
AC_CHECK_HEADERS([pwd.h grp.h sys/param.h])

AC_CONFIG_FILES([Makefile eperl.pod mod/Parse/ePerl.pm])
AC_OUTPUT

>> libeperl.a  # MakeMaker says "Warning (mostly harmless): No library found for -leperl" but then completely removes LIBS= if it doesn't see at least a file here
(cd mod && exec $perl_prog Makefile.PL)
test -s libeperl.a || rm -f libeperl.a
